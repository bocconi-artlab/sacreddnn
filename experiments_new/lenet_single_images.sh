
images=True
loss=mseregr

dset=cifar10
seeds=(9)
opt=sgd
lrs=(0.01)
gpu=0
epochs=4
earlystop=True

for lr in ${lrs[@]}; do
    echo $lr
    for seed in ${seeds[@]}; do
        echo $seed
        python ../dnn.py -F results/lenet_$dset with model=lenet \
        dataset=$dset preproc=1 loss=$loss\
        opt=$opt lr=$lr droplr=0.0 bs=128 wd=0.0 \
        earlystop=$earlystop epochs=$epochs seed=$seed gpu=$gpu \
        images=$images load_model=results/lenet_${dset}/1/model_final.pt \
        logtime=2 deterministic=True save_model=True
    done
done
