
images=True

dset=mnist
seeds=(9)
opt=adam
lrs=(0.001)
gpu=3
epochs=100
earlystop=True

for lr in ${lrs[@]}; do
    echo $lr
    for seed in ${seeds[@]}; do
        echo $seed
        python ../dnn.py -F results/mlp_$dset with model=mlp_1024_1024_1024 bias=False \
        dataset=$dset preproc=1 \
        opt=$opt lr=$lr droplr=0.0 bs=128 wd=0.0 \
        earlystop=$earlystop epochs=$epochs seed=$seed gpu=$gpu \
        images=$images load_model=results/mlp_${dset}/1/model_final.pt \
        logtime=2 deterministic=True save_model=True
    done
done