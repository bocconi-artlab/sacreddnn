dset=cifar10
seeds=(9)
opt=sgd
lrs=(0.01)
gpu=0
epochs=300
earlystop=True

for lr in ${lrs[@]}; do
    echo $lr
    for seed in ${seeds[@]}; do
        echo $seed
        python ../dnn.py -F results/lenet_$dset with model=lenet \
        dataset=$dset preproc=1 \
        opt=$opt lr=$lr droplr=0.0 bs=128 wd=0.0 \
        earlystop=$earlystop epochs=$epochs seed=$seed gpu=$gpu \
        logtime=2 deterministic=True save_model=True
    done
done
