import torch, os
from iterative_magnitude_pruning import prune_magnitude_global
from ..src.get_models import get_model_type

#from ..dnn import main

pruning_iters = 8
pct = 0.2
# define class args


use_cuda = not args.no_cuda and torch.cuda.is_available()
device = torch.device(args.gpu) if use_cuda else torch.device("cpu")
torch.set_num_threads(args.nthreads)
print(f"USE_CUDA = {use_cuda}, DEVICE_COUNT={torch.cuda.device_count()}, NUM_CPU_THREADS={torch.get_num_threads()}")

# define model
Net = get_model_type(args)
model = Net().to(device)

# start from a rewind point -> train a model up to epoch 5
model_name = "resnet20"
dset = "cifar10"
opt = "nesterov"
droplr = 0.0
lr = 0.1
gpu = 0
seed = 2
epochs=5


cmd = f"python ../dnn.py -F results/{model_name}_{dset} with model={model_name} \
        dataset={dset} preproc=2 \
        opt={opt} lr={lr} mom=0.9 droplr={droplr} bs=128 wd=1e-4 rho=0.05 \
        earlystop=False epochs={epochs} seed={seed} gpu={gpu} \
        logtime=2 deterministic=True save_model=True"
os.system(cmd)

epochs=160
# train up to epoch 160
cmd = f"python ../dnn.py -F results/{model_name}_{dset} with model={model_name} \
        dataset={dset} preproc=2 \
        opt={opt} lr={lr} mom=0.9 droplr={droplr} bs=128 wd=1e-4 rho=0.05 \
        earlystop=False epochs={epochs} seed={seed} gpu={gpu} \
        logtime=2 deterministic=True save_model=True"
os.system(cmd)


for _ in range(pruning_iters):
    load_rewind_model = ""
    load_final_model = ""

    rewind_point = model.load_state_dict(torch.load(load_rewind_model))
    final_model = model.load_state_dict(torch.load(load_final_model))

    # prune final model and apply the mask to rewind
    prune_magnitude_global(final_model, rewind_point, pct)

    # retrain
    cmd = f"python ../dnn.py -F results/{model_name}_{dset} with model={model_name} \
            dataset={dset} preproc=2 \
            opt={opt} lr={lr} mom=0.9 droplr={droplr} bs=128 wd=1e-4 rho=0.05 \
            earlystop=False epochs={epochs} seed={seed} gpu={gpu} \
            logtime=2 deterministic=True save_model=True"
    os.system(cmd)