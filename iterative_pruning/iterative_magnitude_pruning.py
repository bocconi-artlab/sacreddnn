import torch
import torch.nn.utils.prune as prune

def prune_magnitude_global(model,old_state,pct):

    with torch.no_grad():
        parameters_to_prune = []
        for name, module in model.named_modules():
            if (isinstance(module, torch.nn.Linear) or isinstance(module, torch.nn.Conv2d)):
                parameters_to_prune.append((module, 'weight'))
        del parameters_to_prune[-1]       # skip last layer

        # Prune
        prune.global_unstructured(
            parameters_to_prune,
            pruning_method=prune.L1Unstructured,
            amount=pct)

        mask_dict = {}
        for name, module in model.named_modules():
            if module in [module_name[0] for module_name in parameters_to_prune]:
                mask_dict[str(name)] = module.weight_mask.data.clone()
                prune.remove(module, 'weight')                                      # make permanent

        # Rewind and apply masks
        model.load_state_dict(old_state)

        for name, module in model.named_modules():
            if name in mask_dict.keys():
                prune.custom_from_mask(module, name='weight', mask=mask_dict[name])
