## Train a replicated system (robust ensemble)
import torch
import torch.optim as optim
from torch.utils.data import SubsetRandomSampler

import argparse
from tqdm import tqdm
import pandas as pd
import numpy as np
import tempfile
from shutil import copyfile
import time

from sacred import Experiment
from sacred.commands import print_config
ex = Experiment('RobustDNN')
from sacred import SETTINGS 
SETTINGS['CAPTURE_MODE'] = 'no'

# add sacreddnn dir to path
import os, sys
sacreddnn_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, sacreddnn_dir) 

# SacredDNN imports
from src.get_datasets import get_dataset, get_loss_function, get_model_type, get_optimizer
from src.models.custom_activations import replace_relu_
from src.robust import RobustNet, RobustDataLoader, LocalDataLoader
from src.utils import num_params, run_and_config_to_path, file_observer_dir
from src.utils import set_beta, topk_acc, take_n_per_class
from src.utils import  perturb, create_perturb, initialization_rescaling, to_gpuid_string, l2_norm
from src.warmup_scheduler import GradualWarmupScheduler

def train(loss, model, train_loader, optimizer, args, center_nrg=False):

    model.train()
    t = tqdm(train_loader) # progress bar integration
    train_loss, accuracy, accuracy5, ndata = 0, 0, 0, 0
    
    for counter, (data, target) in enumerate(t):

        target = target.to(model.master_device)
        
        if args.local and args.y != 1 and counter % args.gtime == 0:
            model.hard_coupling_loss()
        output = model(data)
        l = loss(output, target)
        if not args.local and args.y != 1 and counter % args.gtime == 0:
            l += model.coupling_loss()

        optimizer.zero_grad()
        l.backward()
        optimizer.step()

        train_loss += l.item()*len(data)
        pred = output.argmax(dim=1, keepdim=True)
        accuracy += pred.eq(target.view_as(pred)).sum().item()
        acc5 = topk_acc(output, target, topk=(5,))
        accuracy5 += acc5[0].item()
        ndata += len(data)

        t.set_postfix(loss=train_loss/ndata, err=100*(1-accuracy/ndata), err5=100*(1-accuracy5/ndata))
    
def eval_loss_and_error(loss, model, loader, args, train=False, comp_ensemble=False):
    
    # t0 = time.time()
    model.eval()
    if not args.model.startswith('binary'):
        center = model.get_or_build_center()
    else:
        center = model.build_binary_center_of_mass()
    center.eval()

    l, accuracy = np.zeros(model.y), np.zeros(model.y)
    center_loss, center_accuracy, center_accuracy5 = 0., 0., 0.
    ensemble_loss, ensemble_accuracy = 0., 0.
    ndata = 0
    # committee_loss, committee_accuracy = 0., 0. # TODO

    with torch.no_grad():
        for data, target in loader.single_loader():
            # single replicas
            outputs = model(data, split_input=False, concatenate_output=False)
            target = target.to(model.master_device)
            for a, output in enumerate(outputs):
                l[a] += loss(output, target, reduction='sum').item()
                pred = output.argmax(dim=1, keepdim=True)
                accuracy[a] += pred.eq(target.view_as(pred)).sum().item()
            
            if comp_ensemble:
                # ensemble 
                output = torch.mean(torch.stack(outputs), 0)
                ensemble_loss += loss(output, target, reduction='sum').item()
                pred = output.argmax(dim=1, keepdim=True)
                ensemble_accuracy += pred.eq(target.view_as(pred)).sum().item()

            # center
            output = center(data.to(model.master_device))
            center_loss += loss(output, target, reduction='sum').item()
            pred = output.argmax(dim=1, keepdim=True)
            center_accuracy += pred.eq(target.view_as(pred)).sum().item()
            if not train:
                acc5 = topk_acc(output, target, topk=(5,))
                center_accuracy5 += acc5[0].item()
            ndata += len(data) 


    # print(f"@eval time: {time.time()-t0:.4f}s")
    l /= ndata
    accuracy /= ndata
    center_loss /= ndata
    center_accuracy /= ndata
    center_accuracy5 /= ndata

    num_params_center = sum(p.numel() for p in center.parameters())
    norm_center = 0.0
    for wc in center.parameters():
        norm_center += wc.norm()**2
    norm_center = np.sqrt(norm_center.item() / num_params_center)
    
    if comp_ensemble:
        ensemble_loss /= ndata
        ensemble_accuracy /= ndata
        return l, (1-accuracy)*100, center_loss, (1-center_accuracy)*100,\
            ensemble_loss, (1-ensemble_accuracy)*100, norm_center
    else:
        return l, (1-accuracy)*100, center_loss, (1-center_accuracy)*100, (1-center_accuracy5)*100, norm_center

@ex.config  # Configuration is defined through local variables.
def cfg():

    datapath      = '~/data/'   # folder containing the datasets (e.g. mnist will be in "data/MNIST")
    dataset       = "fashion"   # dataset  [mnist, fashion, cifar10, cifar100]
    preproc       = 0           # data preprocessing level. preprocess=0 is no preproc. Check preproc_transforms.py
    augm_type     = None        # non-trivial data augmentations ((Fast)AutoAugment, CutOut)
    cutout        = 0

    model         = "lenet"     # model type  [lenet, densenet, resnet_cifar, efficientnet-b{1-7}(-pretrained)]  
    act           = 'relu'      # Change activation to e.g. "swish" to replace each relu of the model with a new activation
                                # ["swish", "quadu", "mish", ...]

    loss          = "nll"       # classification loss [nll, mse]
    opt           = "nesterov"  # optimizer type
    lr            = 0.1         # learning rate
    bs            = 128         # input batch size for training
    wd            = 0.          # weight decay param (=L2 reg. Good value is 5e-4)
    mom           = 0.9         # momentum
    dropout       = 0.          # dropout

    droplr        = 10          # learning rate drop factor (use 0 for no-drop)
    tdrop         = 2           # epoch interval at which dropping the learning rate
    drop_mstones  = "drop_1"    # learning rate milestones (epochs at which applying the drop factor)
    warmup        = False       # GradualWarmupScheduler

    epochs        = 100         # number of epochs to train
    
    no_cuda       = False       # disables CUDA training
    nthreads      = 2           # number of threads
    save_model    = False       # save current model to path
    save_epoch    = -1          # save every save_epoch model
    load_model    = ""          # load model from path
    last_epoch    = 0           # last_epoch for schedulers
    
    logtime       = 2           # report every logtime epochs
    gpu           = 0           # which gpu(s) to use, if 'distribute' all gpus are used
    deterministic = True        # set deterministic mode on gpu

    pclass = -1                 # take only pclass training examples for each class 
    #noise_ampl = 0.            # noise amplitude (for perturbing model initialization)
    #init_rescale = 0.          # rescaling of model initialization
    #d0 = 0.                    # for a final replica's fixed distance

    # for binary models
    beta          = 1.
    dbeta         = 0.
    
    ## ROBUST ENSEMBLE SPECIFIC
    y             = 1           # number of replicas
    use_center    = False       # use a central replica
    g             = None        # initial coupling value. If None, balance training and coupling loss at epoch 0
    grate         = None        # coupling increase rate
    gmax          = 1e4         # total coupling multiplicative factor
    gsched        = "exp"       # exp, lin, cosine
    gtime         = 1
    local         = False       # True performs local-SGD not replicated-SGD
    tpost         = 0           # if > 0 post-local SGD starts at epoch tpost
    
    #center_nrg = False
    #comp_ensemble = False         # compute ensemble observables
    #rescale_coupling_loss = False # rescale initial coupling loss at 1
    
@ex.automain
def main(_run, _config):
    # BOOKKEEPING
    args = argparse.Namespace(**_config)
    print_config(_run); print()
    logdir = file_observer_dir(_run)
    if logdir is not None:
        from torch.utils.tensorboard import SummaryWriter
        name_file = run_and_config_to_path(_run, _config)[27:]
        writer = SummaryWriter(log_dir=f"{logdir}/{name_file}")

    # make temp file. In the end, the model will be stored by the observers.
    if args.save_model: 
        save_prefix = tempfile.mkdtemp() + "/model"

    use_cuda = not args.no_cuda and torch.cuda.is_available()
    if use_cuda:
        if args.gpu=="distribute":
            ndevices = torch.cuda.device_count()
            devices = [torch.device(y%ndevices) for y in range(args.y+1)]
        elif isinstance(args.gpu, int):
            devices = [torch.device(args.gpu)]*(args.y+1)
        else:
            if not isinstance(args.gpu, list):
                raise ValueError("please provide gpu as list")
            l=len(args.gpu)
            devices = [torch.device(args.gpu[r%l]) for r in range(args.y+1)]
    else:
        devices = [torch.device("cpu")]*(args.y+1)
    for r, device in enumerate(devices):
        print("{} on {}".format("replica {}".format(r) if r<len(devices)-1 else "master", device))

    torch.manual_seed(args.seed)
    np.random.seed(args.seed)
    if args.deterministic:
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    
    torch.set_num_threads(args.nthreads)

    ## LOAD DATASET
    #loader_args = {'pin_memory': True} if use_cuda else {}
    loader_args = {}
    dtrain, dtest = get_dataset(args)
    if args.pclass > 0:
        train_idxs = take_n_per_class(dtrain, args.pclass)
    else:
        train_idxs = list(range(len(dtrain)))
    test_idxs = list(range(len(dtest)))
    
    print(f"DATASET {args.dataset}: {len(train_idxs)} Train and {len(test_idxs)} Test examples")

    if args.local:
        train_loader = LocalDataLoader(dtrain, batch_size=args.batch_size, shuffle=True, 
            y=args.y, concatenate=True, num_workers=0, **loader_args)
    else:
        train_loader = RobustDataLoader(dtrain, y=args.y, concatenate=True, num_workers=0, batch_size=args.bs,
                                    sampler=SubsetRandomSampler(train_idxs), **loader_args)
    test_loader = RobustDataLoader(dtest, y=args.y, concatenate=True, num_workers=0, batch_size=args.bs,
                                   sampler=SubsetRandomSampler(test_idxs), **loader_args)
    
    ## BUILD MODEL
    Net = get_model_type(args)
    model = RobustNet(Net, y=args.y, g=args.g, grate=args.grate, gmax=args.gmax, 
                      devices=devices, use_center=args.use_center, Tmax=args.epochs)
    #replace activation function
    if args.act not in ['relu', None]:
        replace_relu_(model, args.act)

    # TAKE CARE OF REPLICATED PARAMETERS
    args.lr *= args.y
    # rescale g
    if args.y > 1 and args.g is not None and args.g != 0.:
        gfactor = model.coupling_loss().item() / args.g
        print("dist_0=%s"%(gfactor))
    #    model.multiply_g(1./gfactor)
    #    model.multiply_g(1./args.bs)
        
    # rescale initialization
    #if args.init_rescale > 0.:
    #    initialization_rescaling(model, args.init_rescale)
    # perturb initialization
    #if args.noise_ampl > 0:
    #    z = create_perturb(model)
    #    perturb(model, z, args.noise_ampl)
    #    #ampl = args.noise_ampl * l2_norm(model, mediate=True)
    #    #perturb(model, z, ampl)

    if args.load_model:
        model.load_state_dict(torch.load(args.load_model + ".pt"))
    ex.info["num_params"] = num_params(model)
    print(f"MODEL: {ex.info['num_params']} params")
    
    ## CREATE OPTIMIZER
    optimizer = get_optimizer(args, model)
        
    if args.droplr:
        
        if args.droplr == 'cosine':
            scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=args.epochs, eta_min=0., last_epoch=-1)
            print("Cosine lr schedule")
            
        elif args.droplr < 1. and args.droplr != 0.:
            scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=args.tdrop, gamma=args.droplr)
        else:
            gamma_sched = 1. / args.droplr if args.droplr > 0 else 1
            if args.drop_mstones is None:
                mstones = [args.epochs//2, args.epochs*3//4, args.epochs*15//16]
            else:
                mstones = [int(h) for h in args.drop_mstones.split('_')[1:]]
                if args.model == 'binaryvgg':
                    mstones = [40, 80, 100, 120, 140]
                    #mstones = [120, 240, 300, 360, 420] for 450 epochs
                elif args.model == 'binaryresnet18':
                    mstones = [101, 142, 184, 220]
            print("LR MileStones %s" % mstones)
            print("Custom lr schedule")
                
            scheduler = optim.lr_scheduler.MultiStepLR(optimizer,\
                        milestones=mstones, gamma=gamma_sched)
    else:
        scheduler = None

    if args.warmup:
        scheduler = GradualWarmupScheduler(
            optimizer,
            multiplier=1,
            total_epoch=5,
            after_scheduler=scheduler)

    ## LOSS FUNCTION
    loss = get_loss_function(args)    
    
    ## REPORT CALLBACK
    def report(epoch, esgd_dist=0, esgd_grad=0, lr_out=0, g_esgd=0):

        model.eval()
 
        o = dict() # store scalar observations
        oo = dict() # store array observations
        o["epoch"] = epoch
        if True:
            oo["train_loss"], oo["train_error"], o["train_center_loss"], o["train_center_error"], _, o["norm_center"] = eval_loss_and_error(loss, model, train_loader, args)
            oo["test_loss"], oo["test_error"], o["test_center_loss"], o["test_center_error"], o["test_center_top5_error"], o["norm_center"] = eval_loss_and_error(loss, model, test_loader, args)
        else: # compute ensemble
            oo["train_loss"], oo["train_error"], o["train_center_loss"], o["train_center_error"],\
                o["train_ensemble_loss"], o["train_ensemble_error"], o["norm_center"] = \
                    eval_loss_and_error(loss, model, train_loader, args, comp_ensemble=True)
            oo["test_loss"], oo["test_error"], o["test_center_loss"], o["test_center_error"],\
                o["test_ensemble_loss"], o["test_ensemble_error"], o["norm_center"] = \
                    eval_loss_and_error(loss, model, test_loader, args, comp_ensemble=True)

        o["dist_loss"] = model.distance_loss().item()
        oo["distances"] = np.sqrt(np.array([d.item()/model.num_params() for d in model.sqdistances()]))
        o["coupl_loss"] = model.g * o["dist_loss"]

        if args.model.startswith('binary'):
            o["dist_hamming_loss"] = model.hamming_distance_loss().item()
            oo["distances_hamming"] = np.array([d.item()/model.num_params() for d in model.hamming_distances()])
            o["coupl_hamming_loss"] = model.g * o["dist_hamming_loss"]

        oo["norms"] = np.sqrt(np.array([sqn.item()/model.num_params() for sqn in model.sqnorms()]))
        o["gamma"] = model.g
        if args.droplr:
            o["lr"] = scheduler.get_last_lr()[0]
        else:
            o["lr"] = args.lr

        print("\n", pd.DataFrame({k:[o[k]] for k in o}), "\n")
        for k in o:
            ex.log_scalar(k, o[k], epoch)
            if logdir and epoch > 0:
                writer.add_scalar(k, o[k], epoch)
        for k in oo:
            print(f"{k}:\t{oo[k]}")
            ex.log_scalar(k, np.mean(oo[k]), epoch) # Ref. https://github.com/IDSIA/sacred/issues/465
            if logdir and epoch > 0:
                writer.add_scalar(k, np.mean(oo[k]), epoch)
        print()
        return o, oo

    ## INIT GAMMA IF NEEDED
    if args.g is None: # balance coupling and data loss
        model.g = 0
        o, oo = report(args.last_epoch)
        model.g = np.mean(oo["train_loss"]) / o["dist_loss"]
        #model.g = 0.01654
    else:
        report(args.last_epoch)
    model.g *= args.gtime
    #model.g /= args.bs
    
    if args.grate is None:
        model.grate = args.gmax**(1/args.epochs) - 1
        #model.grate=0.026664
        
    postlocal_gtime = args.gtime

    # dummy loop to make the schedulers progress. TODO: deprecate
    for i in range(args.last_epoch):
        scheduler.step()
        if args.gsched == 'exp':
            model.increase_g()
        elif args.gsched == 'lin':
            model.increase_g_lin()
        elif args.gsched == 'cosine':
            model.increase_g_cosine()
    if args.gsched == 'cosine': # another one for cosine
        model.increase_g_cosine()
        
    print(f"# COUPLING SCHEDULE  g0: {model.g}  grate: {model.grate}")
        
    ## START TRAINING
    for epoch in range(args.last_epoch + 1, args.epochs + 1):

        train(loss, model, train_loader, optimizer, args, center_nrg=False)

        if epoch % args.logtime == 0:
            report(epoch)
            
        # torch.cuda.empty_cache()
        if epoch % args.save_epoch == 0 and args.save_model and args.save_epoch > 0:
            model_path = save_prefix+".pt"
            torch.save(model.state_dict(), model_path)
            kept_model_path = save_prefix+"_epoch_{}.pt".format(epoch)
            copyfile(model_path, kept_model_path)
            ex.add_artifact(kept_model_path)#, content_type="application/octet-stream")
            ex.add_artifact(model_path)#, content_type="application/octet-stream")
            
        #schedulers
        if args.gsched == 'exp':
            model.increase_g()
        elif args.gsched == 'lin':
            model.increase_g_lin()
        elif args.gsched == 'cosine':
            model.increase_g_cosine()
            
        # POST-LOCAL SGD schedule
        if args.tpost != 0 and args.epoch <= args.tpost:
            args.gtime = 1
        elif args.tpost != 0 and args.epoch > args.tpost:
            args.gtime = postlocal_gtime

        # beta scheduler
        if args.dbeta > 0.:
            beta_n = args.beta * (1+args.dbeta)**epoch
            set_beta(model, beta=beta_n)
            
        if args.y == 1 and args.model.startswith('resnet110') and args.droplr != 'cosine': 
            # warm-up for ResNet-110 (single)
            mstones = [int(h) for h in args.drop_mstones.split('_')[1:]]
            if epoch == 1:
                print("warm-up for resnet-110: lr %s" % args.lr)
                args.lr = 0.1
                for param_group in optimizer.param_groups:
                    param_group['lr'] = args.lr
            elif epoch == mstones[0]:
                args.lr = 0.01
                for param_group in optimizer.param_groups:
                    param_group['lr'] = args.lr
            elif epoch == mstones[1]:
                args.lr = 0.001
                for param_group in optimizer.param_groups:
                    param_group['lr'] = args.lr
        else:
            if args.droplr:
                scheduler.step()
                print("lr=%s" % scheduler.get_last_lr())

    # Save model after training
    if args.save_model:

        model_path = save_prefix+"_final.pt"
        center = model.get_or_build_center()
        torch.save(center.state_dict(), model_path)
        ex.add_artifact(model_path)#, content_type="application/octet-stream")

        model_path_replicas = save_prefix+"_replicas_final.pt"
        torch.save(model.state_dict(), model_path_replicas)
        ex.add_artifact(model_path_replicas)#, content_type="application/octet-stream")
