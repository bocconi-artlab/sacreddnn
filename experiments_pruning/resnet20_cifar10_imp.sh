model=resnet20
dset=cifar10
seeds=(9)
opt=nesterov
lrs=(0.1)
gpu=2
epochs=160
earlystop=False

prune_rep=3
prune_init=4
prune_ep=3
prune_pct=0.2



for lr in ${lrs[@]}; do
    echo $lr
    for seed in ${seeds[@]}; do
        echo $seed
        python ../dnn.py -F results/${model}_$dset with model=$model \
        dataset=$dset preproc=2 \
        opt=$opt lr=$lr mom=0.9 droplr=cosine bs=128 wd=1e-4 \
        earlystop=$earlystop epochs=$epochs seed=$seed gpu=$gpu \
        logtime=2 deterministic=True save_model=True \
        prune_rep=$prune_rep prune_init=$prune_init prune_ep=$prune_ep prune_pct=$prune_pct
    done
done
