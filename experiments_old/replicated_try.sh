seeds=(2)
dset=cifar10
lrs=(0.02)
gpu=2
ys=(3)
gtime=(1)
g=None
wd=0.0001

for y in ${ys[@]}; do
    echo $y
    for lr in ${lrs[@]}; do
        echo $lr
        for seed in ${seeds[@]}; do
            echo $seed
            python ../scripts/robust_dnn.py -F runs/local_sgd with local=True seed=$seed dataset=$dset preproc=2 wd=$wd \
            model=resnet20 use_center=True logtime=2 save_model=False y=$y g=$g gtime=$gtime bs=128 drop_mstones=drop_150_225 \
            opt=nesterov droplr=10. lr=$lr gpu=$gpu epochs=300 deterministic=True
	    done
    done
done

