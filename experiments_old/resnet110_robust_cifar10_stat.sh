seeds=(3)
dset=cifar10
lrs=(1.)
ys=(3)
gpu=0
gfactor=1e5
deterministic=False

for y in ${ys[@]}; do
    echo y=$y
    for lr in ${lrs[@]}; do
        echo lr=$lr
        for seed in ${seeds[@]}; do
            echo seed=$seed
            python3 scripts/robust_dnn.py -F runs/resnet110_${dset}_may with gfactor=$gfactor seed=$seed dataset=$dset preproc=2 \
            model=resnet110 save_model=False bs=128 lr=$lr opt=nesterov droplr=10. drop_mstones=drop_82_124 \
            gpu=$gpu epochs=164 wd=1e-4 deterministic=$deterministic y=$y
        done
    done
done
