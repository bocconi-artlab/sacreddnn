seeds=(2)
lrs=(0.2)
gpu=2
weight_decay=(0.0001)
deterministic=True
preproc=2

for lr in ${lrs[@]}; do
	echo $lr
	for seed in ${seeds[@]}; do
		echo $seed
		python ../scripts/dnn.py -F runs/resnet20_cifar10_single with seed=$seed weight_decay=$weight_decay preproc=$preproc \
		dataset=cifar10 model=resnet20 logtime=2 save_model=False bs=128 opt=nesterov droplr=10 lr=$lr gpu=$gpu epochs=300 \
		deterministic=$deterministic 
	done
done

