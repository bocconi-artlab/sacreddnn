dset=cifar10
seeds=(2)
lrs=(0.01)
gpu=0
y=3
g=None
gfactor=1e5
for lr in ${lrs[@]}; do
    echo $lr
    for seed in ${seeds[@]}; do
        echo $seed
        python ../robust_dnn.py -F runs/resnet18_$dset with model=resnet18 y=$y g=$g \
        dataset=$dset preproc=2 \
        opt=nesterov lr=$lr drop_mstones=drop_110 droplr=10. bs=128 \
        epochs=160 seed=$seed logtime=2 save_model=False gpu=$gpu deterministic=True
    done
done
