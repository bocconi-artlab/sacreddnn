seeds=(2)
dset=cifar10
lrs=(0.01)
gpu=0
y=3
g=None
gfactor=1e5
deterministic=False

for lr in ${lrs[@]}; do
    echo $lr
    for seed in ${seeds[@]}; do
        echo $seed
        python scripts/robust_dnn.py -F runs/resnet18_$dset with seed=$seed dataset=$dset preproc=2 \
        model=resnet18 y=$y g=$g gfactor=$gfactor \
        opt=nesterov lr=$lr droplr=10. drop_mstones=drop_110 bs=128 \
        gpu=$gpu epochs=160 logtime=2 save_model=False deterministic=$deterministic &
    done
done
