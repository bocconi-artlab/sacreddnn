dset=cifar10
seeds=(2)
lrs=(0.01)
gpu=0

for lr in ${lrs[@]}; do
    echo $lr
    for seed in ${seeds[@]}; do
        echo $seed
        python ../scripts/dnn.py -F runs/resnet18_cifar10_single with dataset=$dset preproc=1 model=resnet18 \
        opt=nesterov lr=$lr droplr=0. bs=128 epochs=160 seed=$seed \
        gpu=$gpu logtime=2 deterministic=True save_model=False
    done
done
