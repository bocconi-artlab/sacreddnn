seeds=(8)
dset=cifar100
lrs=(0.1)
ys=(3)
gpu=0
gfactor=1e5
deterministic=False
model=densenet121

for y in ${ys[@]}; do
    echo y=$y
    for lr in ${lrs[@]}; do
        echo lr=$lr
        for seed in ${seeds[@]}; do
            echo seed=$seed
            python scripts/robust_dnn.py -F runs/${model}_${dset}_may with gfactor=$gfactor seed=$seed dataset=$dset preproc=2 model=$model \
            save_model=False bs=64 lr=$lr opt=nesterov droplr=10. drop_mstones=drop_150_225 gpu=$gpu epochs=300 \
            wd=1e-4 deterministic=$deterministic y=$y
        done
    done
done
