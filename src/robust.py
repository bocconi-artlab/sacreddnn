import torch
import math, random
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, Subset
from .binarynet import modules
binarize = modules.BinarizeF.apply

class RobustDataLoader():
    def __init__(self, dset, y, concatenate, M=-1, **kwargs):
        if M > 0:
            dset = Subset(dset, range(min(M, len(dset))))
        self.y = y
        self.dls = [DataLoader(dset, **kwargs) for a in range(y)]
        self.concatenate = concatenate
        self.dataset = dset

    def __iter__(self):
        if self.concatenate:
            for xs in zip(*self.dls):
                yield tuple(torch.cat([x[i] for x in xs]) for i in range(len(xs[0])))
        else:
            for xs in zip(*self.dls):
                yield xs

    def __len__(self):
        return len(self.dls[0])

    def single_loader(self):
        return self.dls[0]

# TODO: da rivedere
class LocalDataLoader():
    def partition(self, dset, y):
        indices = [x for x in range(0, len(dset))]
        random.shuffle(indices)
        partitions = []
        partition_sizes = [1/y for _ in range(y)]
        from_index = 0
        for partition_size in partition_sizes:
            to_index = from_index + int(partition_size * len(dset))
            partitions.append(indices[from_index:to_index])
            from_index = to_index
        dsets = [Subset(dset,partitions[i]) for i in range(y)]
        return dsets
        
    def __init__(self, dset, y, concatenate, **kwargs):
        
        dsets = self.partition(dset,y)
        self.y = y
        self.dls = [DataLoader(dsets[i], **kwargs) for i in range(y)]
        self.concatenate = concatenate
        self.dataset = dset

    def __iter__(self):
        if self.concatenate:
            for xs in zip(*self.dls):
                yield tuple(torch.cat([x[i] for x in xs]) for i in range(len(xs[0])))
        else:
            for xs in zip(*self.dls):
                yield xs

    def __len__(self):
        return len(self.dls[0])

    def single_loader(self): # soprattutto questo credo sia sbagliato
        return self.dls[0] 

class RobustNet(nn.Module):
    def __init__(self, Net, y=3, g=0., grate=1e-2, devices=None, gmax=float("inf"), use_center=False, Tmax=0, last_epoch=0):
            super().__init__()

            self.replicas = nn.ModuleList([Net() for a in range(y)])
            if devices is None:
                self.devices = [torch.device("cpu")]*(y+1)
            else:
                self.devices = devices
            self.distribute()
            self.master_device = self.devices[-1]
            self.g = g           # coupling
            self.grate = grate   # coupling increase rate

            self.gmax = gmax     # maximum coupling
            self.last_epoch = last_epoch
            self.Tmax = Tmax
            self.y = y           # number of replicas
            self.center = Net() if use_center else None
            self.Net = Net

    def distribute(self):
            for r, replica in enumerate(self.replicas):
                replica.to(self.devices[r])

    def forward(self, x, split_input=True, concatenate_output=True):
        if not isinstance(x, tuple) and not isinstance(x, list):
            if split_input:
                x = torch.chunk(x, self.y)
            else: # duplicate input
                x = tuple(x for a in range(self.y))

        x = [x[r].to(self.devices[r]) for r in range(len(x))]
        result = []
        for xr, replica in zip(x, self.replicas):
            result.append(replica(xr))

        result = [r.to(self.master_device) for r in result]

        if concatenate_output: # recompose
            return torch.cat(result)
        else:
            return result

    def has_center(self):
        return not self.center is None

    # num params per replica
    def num_params(self):
        return sum(p.numel() for p in self.replicas[0].parameters())

    def increase_g(self):
        self.g *= 1 + self.grate
        #self.g = min(self.g, self.gmax)
        
    def increase_g_linear(self):
        self.g += self.grate
        #self.g = min(self.g, self.gmax)

    def increase_g_cosine(self):
        self.g = -0.5 * self.gmax * (-1. + math.cos(math.pi * self.last_epoch / self.Tmax))
        self.last_epoch += 1
                
    def coupling_loss(self):
        return 0 if self.g == 0 else self.g * self.distance_loss()
    
    def hard_coupling_loss(self):
        with torch.no_grad():
            center = self.build_center_of_mass()
            for r in self.replicas:
                for wr, wc in zip(r.parameters(), center.parameters()):
                    wr.data.copy_(wc.data.detach())

    def distance_loss(self):
        return torch.mean(torch.stack(self.sqdistances()))

    def hamming_coupling_loss(self):
        return self.g * self.hamming_distance_loss()

    def hamming_distance_loss(self):
        return torch.mean(torch.stack(self.hamming_distances()))
    
    def hamming_distances(self):
        dists = [0.0]*self.y
        for i, wreplicas in enumerate(zip(*(r.named_parameters() for r in self.replicas))):
            wreplicas_master = [binarize(w[1]).to(self.master_device).detach() if hasattr(w[1], 'org') and 'weight' in w[0] else w[1].to(self.master_device).detach() for w in wreplicas]
            if hasattr(wreplicas[0][1], 'org') and 'weight' in wreplicas[0][0]:
                #print("%i\n"%i)
                #wc = torch.mean(torch.stack(wreplicas_master).detach(), 0)
                wc = binarize(torch.sum(torch.stack(wreplicas_master).detach(), 0))
                for a, wr in enumerate(wreplicas):
                    dists[a] += F.mse_loss(wc.to(self.devices[a]), binarize(wr[1]), reduction='sum').to(self.master_device) / 4
            else:
                wc = torch.mean(torch.stack(wreplicas_master).detach(), 0)
                for a, wr in enumerate(wreplicas):
                    dists[a] += F.mse_loss(wc.to(self.devices[a]), wr[1], reduction='sum').to(self.master_device)
        return dists
    
    # distances with the center of mass
    def sqdistances(self):
        dists = [0.0]*self.y
        if self.has_center():
            for a,r in enumerate(self.replicas):
                for wr, wc in zip(r.parameters(), self.center.parameters()):
                    dists[a] += F.mse_loss(wc, wr.to(self.master_device), reduction='sum')
        else:
            for wreplicas in zip(*(r.parameters() for r in self.replicas)):
                wreplicas_master = [w.to(self.master_device).detach() for w in wreplicas]
                wc = torch.mean(torch.stack(wreplicas_master).detach(), 0)
                for a, wr in enumerate(wreplicas):
                    dists[a] += F.mse_loss(wc.to(self.devices[a]), wr, reduction='sum').to(self.master_device)
        return dists

    def coupling_loss_d0(self, d0):
        return self.g * torch.mean(torch.stack(self.sqdistances_d0(d0)))
    
    # distances with the center of mass
    def sqdistances_d0(self, d0):
        dists = [0.0]*self.y
        if not self.has_center():
            for wreplicas in zip(*(r.parameters() for r in self.replicas)):
                wreplicas_master = [w.to(self.master_device).detach() for w in wreplicas]
                wc = torch.mean(torch.stack(wreplicas_master).detach(), 0)
                for a, wr in enumerate(wreplicas):
                    dists[a] += (F.mse_loss(wc.to(self.devices[a]), wr, reduction='sum').to(self.master_device) - d0)**2
        return dists


    def sqnorms(self):
        sqns = [0.0]*self.y
        for wreplicas in zip(*(s.parameters() for s in self.replicas)):
            for a, wr in enumerate(wreplicas):
                sqns[a] += wr.norm()**2
        return sqns

    def build_center_of_mass(self):
        center = self.Net()
        center.to(self.master_device)
        for wc, *wreplicas in zip(center.parameters(), *(r.parameters() for r in self.replicas)):
            wreplicas = [w.to(self.master_device) for w in wreplicas]
            wc.data = torch.mean(torch.stack(wreplicas), 0).data

        for bc, *breplicas in zip(center.buffers(), *(r.buffers() for r in self.replicas)):
            breplicas = [b.to(self.master_device) for b in breplicas]
            if breplicas[0].dtype == torch.long:
                bc.data = torch.ceil(torch.mean(torch.stack(breplicas).double())).long().data
            else:
                bc.data = torch.mean(torch.stack(breplicas), 0).data

        return center

    def get_or_build_center(self):
        if self.has_center():
            return self.center.to(self.master_device)
        else:
            return self.build_center_of_mass()

    def build_binary_center_of_mass(self):
        center = self.Net()
        center.to(self.master_device)
        for i, (wc, *wreplicas) in enumerate(zip(center.parameters(), *(r.named_parameters() for r in self.replicas))):
            if hasattr(wreplicas[0][1], 'org') and 'weight' in wreplicas[0][0]:
                wreplicas = [w[1] for w in wreplicas]
                wreplicas = [binarize(w).to(self.master_device) for w in wreplicas]
                wc.data = binarize(torch.sum(torch.stack(wreplicas).detach(), 0))
            else:
                wreplicas = [w[1] for w in wreplicas]
                wreplicas = [w.to(self.master_device) for w in wreplicas]
                wc.data = torch.mean(torch.stack(wreplicas), 0).data
                   
        for bc, *breplicas in zip(center.buffers(), *(r.buffers() for r in self.replicas)):
            breplicas = [b.to(self.master_device) for b in breplicas]
            if breplicas[0].dtype == torch.long:
                bc.data = torch.ceil(torch.mean(torch.stack(breplicas).double())).long().data
            else:
                bc.data = torch.mean(torch.stack(breplicas), 0).data

        return center
