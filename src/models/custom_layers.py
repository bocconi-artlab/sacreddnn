from turtle import forward
import torch
import torch.nn as nn

class MultiplyLayer(nn.Module):
    def __init__(self, size):
        super().__init__()
        self.size = size
        self.weight = nn.Parameter(torch.Tensor(self.size))
        torch.nn.init.ones_(self.weight)

    def forward(self, x):
        return x * self.weight


class MultiplyLayerColors(nn.Module):
    def __init__(self, size1, size2, size3):
        super().__init__()
        self.weight = nn.Parameter(torch.Tensor(size1, size2, size3))
        torch.nn.init.ones_(self.weight)

    def forward(self, x):
        return x * self.weight
