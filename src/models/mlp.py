import torch
import torch.nn as nn
import torch.nn.functional as F
    
from .custom_layers import MultiplyLayer

class MLP(nn.Module):
    def __init__(self, nin, nhs, nout, bias=True):
        super(MLP, self).__init__()
        assert isinstance(nhs, list)

        self.layers = nn.Sequential(
            nn.Linear(nin, nhs[0], bias=bias),
            nn.ReLU(),
            *[nn.Linear(nhs[i//2], nhs[i//2 + 1], bias=bias) if i%2==0 else nn.ReLU() for i in range(2*(len(nhs)-1))],
            nn.Linear(nhs[-1], nout, bias=bias))
        
    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        return self.layers(x)

class MLP_images(nn.Module):
    def __init__(self, pretrained_model):
        super(MLP_images, self).__init__()
        #self.new_layers = nn.Sequential(nn.Linear(784, 784, bias=True))
        self.new_layers = nn.Sequential(MultiplyLayer(784), nn.Sigmoid())
        self.pretrained = pretrained_model
    
    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        x = self.new_layers(x)
        x = self.pretrained(x)
        return x


"""
class MLP_linear(nn.Module):
    def __init__(self, nin, nhs, nout):
        super(MLP_linear, self).__init__()
        assert isinstance(nhs, list)

        self.layers = nn.Sequential(
            nn.Linear(nin, nhs[0]),
            *[nn.Linear(nhs[i], nhs[i + 1]) for i in range(len(nhs)-1)],
            nn.Linear(nhs[-1], nout))
        
    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        return self.layers(x)
"""