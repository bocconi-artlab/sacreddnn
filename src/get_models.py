import torch.nn as nn
import numpy as np

from .binarynet import binarized_modules, binary_lenet, vgg_cifar10_binary, resnet_binary, vggbin
from .models import lenet, mlp, preactresnet, wideresnet, efficientnet, deep_double_descent
from .models import resnet_cifar, densenet_cifar, vgg, vgg_badsgd, resnet
from .models import torchvision_models
from .models.custom_activations import ReQU, QuadU, Swish, Mish, QuadLin, QuadBald, SlowQuad

###### FAST-AA IMPORTS
from .fast_autoaugment_utils import resnet_fastaa, pyramidnet, wideresnet_aa
######

def get_xsize(args):
    xsize = [28,28]   if args.dataset in ["mnist","fashion"] else \
            [32,32]   if args.dataset in ["cifar10","cifar100","svhn"] else \
            [64,64]   if args.dataset=='tiny-imagenet' else \
            [224,224] if args.dataset=='imagenet' else args.xsize
    return xsize

def get_model_type(args, pretrained_model=None):

    nclasses =  10   if args.dataset in ["mnist","fashion","cifar10", "svhn"] else \
                100  if args.dataset in ["cifar100"] else \
                200  if args.dataset == 'tiny-imagenet' else \
                1000 if args.dataset == 'imagenet' else args.nclasses

    nchannels = 1 if args.dataset in ["mnist", "fashion"] else \
                3 if args.dataset in ["cifar10", "cifar100", "svhn", "tiny-imagenet", "imagenet"] else 3

    xsize = get_xsize(args)
    
    new_act = nn.ReLU()  if args.act == 'relu' else \
              nn.Tanh()  if args.act == 'tanh' else \
              ReQU()     if args.act == 'requ' else \
              QuadU()    if args.act == 'quadu' else \
              Swish()    if args.act == 'swish' else \
              QuadLin()  if args.act == 'quadlin' else \
              QuadBald() if args.act == 'quadbald' else \
              SlowQuad() if args.act == 'slowquad' else \
              Mish()     if args.act == 'mish' else None

    if args.model.startswith('mlp'):
        nin = np.prod(xsize)*nchannels
        nhs = [int(h) for h in args.model.split('_')[1:]]
        if args.model.startswith('mlplinear'):
            Net = lambda: mlp.MLP_linear(nin, nhs, nclasses)
        elif args.model.startswith('mlpimages'):
            Net = lambda: mlp.MLP_images(pretrained_model)
        else:
            Net = lambda: mlp.MLP(nin, nhs, nclasses, bias=args.bias)

    elif args.model.startswith('lenet'):
            if args.model.startswith('lenetimages'):
                Net = lambda: lenet.LeNet_images(pretrained_model)
            else:
                Net = lambda: lenet.LeNet(colors=nchannels)

    #elif args.model in ['lenet', 'lenet_bn', 'lenet5', 'lenet5_bn']:
    #    if xsize[0] != xsize[1]:
    #        raise NotImplementedError("input needs to be square for lenet")
    #    LeNet_type = lenet.LeNet     if args.model == 'lenet'     else \
    #                 lenet.LeNet_bn  if args.model == 'lenet_bn'  else \
    #                 lenet.LeNet5    if args.model == 'lenet5'    else \
    #                 lenet.LeNet5_bn if args.model == 'lenet5_bn' else None
    #    Net = lambda: LeNet_type(dim=xsize[0], in_channels=nchannels, nclasses=nclasses, activation=new_act, dropout=args.dropout)
    #elif args.model == "testlenet5":
    #        Net = lambda: lenet.TestLeNet5()

    elif args.model == 'resnet20':
        Net = lambda: resnet_cifar.ResNet20(num_classes=nclasses)

    elif args.model == 'resnet110':
        #Net = lambda: resnet_cifar.ResNet110(num_classes=nclasses)
        Net = lambda: resnet_cifar.ResNet110(num_classes=nclasses, activation=new_act)

    elif args.model == 'vgg16_bn':
        Net = lambda: vgg.vgg16_bn()

    elif args.model == 'resnet18':
        Net = lambda: resnet.resnet18()

    elif args.model == "badvgg":
        Net = lambda: vgg_badsgd.VGG('VGG16')

    elif args.model == "cnndescent":
            Net = lambda: deep_double_descent.CNNdescent()

    elif args.model == 'densenet100':
        Net = lambda: densenet_cifar.densenet(num_classes=nclasses,depth=100,growthRate=12,dropRate=0.)
    #elif args.model.startswith('wideresnet'): 
    #    depth = int(args.model.split('_')[1])
    #    widen_factor = int(args.model.split('_')[2])
    #    Net = lambda: wideresnet.WideResNet(depth, nclasses, widen_factor)

    elif args.model == 'preactresnet18':
        Net = lambda: preactresnet.PreActResNet18(nclasses)

    # FAST-AA MODELS
    elif args.model == 'resnet110AA':
        Net = lambda: resnet_fastaa.ResNetAA(args.dataset, 110, nclasses, bottleneck=False)
    elif args.model == 'pyramidnetAA':
        Net = lambda: pyramidnet.PyramidNetAA(args.dataset, 272, 200, nclasses, bottleneck=True)
    elif args.model == 'wresnet28_10':
        Net = lambda: wideresnet_aa.WideResNet(28, 10, dropout_rate=0.0, num_classes=nclasses)
    ################
        
    elif args.model.startswith('efficientnet'):
        if args.model.endswith('pretrained'):
            Net = lambda: efficientnet.EfficientNet.from_pretrained(args.model.replace('-pretrained', ''), num_classes=nclasses)
        else:
            Net = lambda: efficientnet.EfficientNet.from_name(args.model, override_params={"num_classes": nclasses})
    
    elif args.model=='binarylenet':
        Net = lambda: binary_lenet.BinaryLeNet(dim=xsize[0], in_channels=nchannels)
        
    elif args.model=='binaryvgg':
        Net = lambda: vgg_cifar10_binary.vgg_cifar10_binary(num_classes=nclasses, dropout=args.dropout)

    elif args.model=='binaryvgg_courba':
        Net = lambda: vggbin.Model()

    elif args.model=='binaryresnet18':
        Net = lambda: resnet_binary.resnet_binary(num_classes=nclasses, depth=18, dataset=args.dataset)
        
    else: # try if corresponds to one of the models import from torchvision (see the file models/torchvision_models.py)
        print("LOOKING IN TORCHVISION MODELS")
        Net = lambda: eval(f"torchvision_models.{args.model}")(num_classes=nclasses)

    return Net

    """
    elif args.model == 'pratiklenet':
        # pratik LeNet
        class View(nn.Module):
            def __init__(self,o):
                super(View, self).__init__()
                self.o = o
            def forward(self,x):
                return x.view(-1, self.o)
            
        def convbn(ci,co,ksz,psz,p):
            return nn.Sequential(
                nn.Conv2d(ci,co,ksz),
                nn.BatchNorm2d(co),
                nn.ReLU(True),
                nn.MaxPool2d(psz,stride=psz),
                nn.Dropout(p))
        
        Net = lambda : nn.Sequential(
            convbn(1,20,5,3,0.25),
            convbn(20,50,5,2,0.25),
            View(50*2*2),
            nn.Linear(50*2*2, 500),
            nn.BatchNorm1d(500),
            nn.ReLU(True),
            nn.Dropout(0.25),
            nn.Linear(500,10),
            )
"""