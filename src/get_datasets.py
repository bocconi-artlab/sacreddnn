from torch.utils.data import ConcatDataset
from torchvision import datasets, transforms

from .preproc_transforms import preproc_transforms

def return_index(cls):              # Update dataset class to return indicies. Data and label as tupel, second object index
    def __getitem__(self, index):
        data, target = cls.__getitem__(self, index)
        return data, target, index

    return type(cls.__name__, (cls,), {
        '__getitem__': __getitem__, })

def get_dataset(args):

    if args.dataset == 'mnist':
        DSet = datasets.MNIST
    elif args.dataset == 'fashion':
        DSet = datasets.FashionMNIST
    elif args.dataset == 'cifar10':
        DSet = datasets.CIFAR10
    elif args.dataset == 'cifar100':
        DSet = datasets.CIFAR100
    elif args.dataset == 'svhn':
        DSet = datasets.SVHN
    elif args.dataset == 'tiny-imagenet':
        #train_dir = '~/data/tiny-imagenet-200/train'
        #val_dir = '~/data/tiny-imagenet-200/val'
        train_dir = '/home/pittorino/data/tiny-imagenet-200/train'
        val_dir = '/home/pittorino/data/tiny-imagenet-200/val/images'
        DSet = datasets.ImageFolder
    elif args.dataset == 'imagenet':
        train_dir = '/home/Datasets/ImageNet/ILSVRC/Data/CLS-LOC/train'
        val_dir = '/home/Datasets/ImageNet/ILSVRC/Data/CLS-LOC/val'
        DSet = datasets.ImageFolder

    if args.preproc:
        transform_train, transform_test = preproc_transforms(args)
    else:
        transform_train = transforms.ToTensor()
        transform_test = transforms.ToTensor()
               
    if args.dataset not in ['tiny-imagenet', 'imagenet', "svhn"]:


        #dtrain = DSet(args.datapath, train=True, download=True, transform=transform_train)
        #dtest = DSet(args.datapath, train=False, download=True, transform=transform_test)
        
        dtrain = return_index(datasets.__dict__["CIFAR10"])(
            root=args.datapath, 
            train=True, 
            download=True,
            transform=transform_train,)
        dtest = return_index(datasets.__dict__["CIFAR10"])(
            root=args.datapath, 
            train=False, 
            download=True,
            transform=transform_test,)


    elif args.dataset == "svhn": 
        #trainset = DSet(args.datapath, split='train', download=True, transform=transform_train)
        #extraset = DSet(args.datapath, split='extra', download=True, transform=transform_train)
        #dtrain = ConcatDataset([trainset, extraset])
        dtrain = DSet(args.datapath, split='train', download=True, transform=transform_train)
        dtest = DSet(args.datapath, split='test', download=True, transform=transform_test)  
    else:
        #dtrain = DSet(train_dir, transform=transform_train)
        #dtest = DSet(val_dir, transform=transform_test)   
    
        dtrain = return_index(datasets.ImageFolder)(
            root='/home/pittorino/data/tiny-imagenet-200/train', 
            transform=transform_train,)
        dtest = return_index(datasets.ImageFolder)(
            root='/home/pittorino/data/tiny-imagenet-200/val/images',             # test
            transform=transform_test,)



    return dtrain, dtest
