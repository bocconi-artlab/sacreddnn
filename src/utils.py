import torch
from   sacred.observers import FileStorageObserver
import numpy as np
from src.models.custom_activations import HardtanhBeta, TanhBeta


def predict_outputs(model, dataloader, device):
    all_outputs = torch.tensor([],device=device)
    model.eval()
    with torch.no_grad():
        for data,_,_ in dataloader:
            data = data.to(device)
            pred = model(data)
            all_outputs = torch.cat((all_outputs, pred), 0)
    return all_outputs.cpu()

# https://discuss.pytorch.org/t/convert-int-into-one-hot-format/507/29
def to_onehot(idx, K):
    x = torch.zeros(len(idx), K, device=idx.device)
    return x.scatter_(1, idx.unsqueeze(1), 1.)

def num_params(model, learnable=False):
    if learnable:
        return sum(p.numel() for p in model.parameters() if p.requires_grad==True)
    else:
        return sum(p.numel() for p in model.parameters())

def l2_norm(model, mean=False):
    if mean:
        return torch.sqrt(sum(p.norm()**2 for p in model.parameters()) / num_params(model)).item()
    else:
        return torch.sqrt(sum(p.norm()**2 for p in model.parameters())).item()

def run_and_config_to_path(_run, _config):
    path = f"{_run.experiment_info['name']}"
    exclude_list =  ["logtime", "gpu", "nthreads", "save_model", "no_cuda", "load_model"]
    exclude_list += ["deterministic", "save_epoch", "last_epoch", "use_center"]
    exclude_list += ["cutout", "augm_type"]
    exclude_list += ["drop_mstones", "beta", "dbeta"]
    for k in _config:
        if k not in exclude_list:
            path += f"_{k}={_config[k]}"
    return path

def file_observer_dir(_run):
    for o in _run.observers:
        if isinstance(o, FileStorageObserver):
            return o.dir
    return None

def to_gpuid_string(x):
    if isinstance(x, list):
        return str(x)[1:-1]
    return str(x)

# Returns indexes of n examples for each class
def take_n_per_class(dataset, n):
    y = dataset.targets
    classes = y.unique()
    idxs = []
    print(f"Classes {len(classes)}")
    for c in classes:
        (idxs_c, ) = torch.where(y == c)
        idxs_c = np.random.permutation(idxs_c)[:n]
        # print(f"Class {c}: {len(idxs_c)}")
        idxs.extend(idxs_c.tolist())
    return idxs

def set_beta(model, beta=1.):
    for name, child in model.named_children():
        if isinstance(child, HardtanhBeta) or isinstance(child, TanhBeta):
            print("ciao beta_n=%s"%beta)
            child.beta = beta
        set_beta(child, beta=beta)

def topk_acc(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        #batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].reshape(-1).float().sum(0, keepdim=True)
            #res.append(correct_k.mul_(100.0 / batch_size))
            res.append(correct_k)
        return res

def create_perturb(model):
    z = []
    for p in model.parameters():
        r = p.clone().detach().normal_()
        z.append(p.data * r)
    return z
    
def perturb(model, z, noise_ampl):
    for i,p in enumerate(model.parameters()):
        p.data += noise_ampl * z[i]

def initialization_rescaling(model, gain_factor):
    for p in model.parameters():
        p.data *= gain_factor