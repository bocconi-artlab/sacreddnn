import torch
import torch.nn.functional as F

from .binarynet import binarized_modules
from .utils import to_onehot

class KD_loss(torch.nn.Module):
    def __init__(self, alpha, T):
        super().__init__()
        self.alpha = alpha
        self.T = T
    
    def forward(self, outputs, labels, teacher_outputs):
        KD_loss = self.alpha * torch.nn.functional.cross_entropy(outputs, labels) + \
                  (1. - self.alpha) * (self.T **2) * \
                  torch.nn.KLDivLoss(reduction='batchmean')(torch.nn.functional.log_softmax(outputs/self.T, dim=1), \
                  torch.nn.functional.softmax(teacher_outputs/self.T, dim=1))
        return KD_loss


def KD_loss_fn(outputs, labels, teacher_outputs, alpha, T):
    KD_loss = alpha * torch.nn.functional.cross_entropy(outputs, labels) + \
                (1. - alpha) * (T **2) * \
                torch.nn.KLDivLoss(reduction='batchmean')(torch.nn.functional.log_softmax(outputs / T, dim=1), \
                torch.nn.functional.softmax(teacher_outputs / T, dim=1))
    return KD_loss


def get_loss_function(args):
    
    if not args.load_teacher:

        if args.loss == 'nll':
            def loss(output, target, **kwargs):
                output = F.log_softmax(output, dim=1)
                return F.nll_loss(output, target, **kwargs)
                #myloss = nn.CrossEntropyLoss()
                #return myloss(output, target)

        # mse for classification
        elif args.loss == 'mse':
            def loss(output, target, **kwargs):
                target = to_onehot(target, output.shape[1])
                return F.mse_loss(output, target, **kwargs)

        # mse for regression
        elif args.loss == 'mseregr':
            def loss(output, target, **kwargs):
                return F.mse_loss(output, target, **kwargs)

        elif args.loss == 'hinge': #TODO
            def loss(output, target, **kwargs):
                target = to_onehot(target, output.shape[1])
                
                #target=target.unsqueeze(1)
                #target_onehot = torch.cuda.FloatTensor(target.size(0), 10)
                #target_onehot.fill_(-1)
                #target_onehot.scatter_(1, target, 1)
                #target=target.squeeze()
                
                myloss = binarized_modules.HingeLoss()
                return myloss(output, target, **kwargs)
    
    else:

        def loss(output, target, teacher_outputs, alpha, T):
            return KD_loss_fn(output, target, teacher_outputs, alpha, T)
        
    return loss