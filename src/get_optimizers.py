import torch.optim as optim

###### FAST-AA IMPORTS
from .entropy_sgd import EntropySGD
from .fast_autoaugment_utils.rmspropTF import RMSpropTF
from .SAM.sam import SAM
######

def get_optimizer(args, model):

    if args.opt == "adam":
        optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.wd)
    elif args.opt == "sgd":
        optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.mom, nesterov=False, weight_decay=args.wd)
    elif args.opt == "nesterov":
        optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.mom, nesterov=True, weight_decay=args.wd)
    elif args.opt == "rmsprop": # optimizer TensorFlow style
        optimizer = RMSpropTF(model.parameters(), lr=args.lr, weight_decay=args.wd, alpha=0.9, momentum=0.9, eps=0.001)

    elif args.opt == "SAM":
        base_optimizer = optim.SGD
        optimizer = SAM(model.parameters(), base_optimizer, rho=args.rho, lr=args.lr, momentum=args.mom, weight_decay=args.wd)
        
        #lr_scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer.base_optimizer, T_max=args.epochs)

    elif args.opt.startswith('entropy-sgd'):
        if args.dataset.startswith('cifar'):
            num_batches = int(5e4/args.bs)+1
        elif args.dataset in ["mnist", "fashion"]:
            num_batches = int(6e4/args.bs)+1
        elif args.dataset in ["svhn"]:
            num_batches = int(73257/args.bs)+1
        elif args.dataset in ["tiny-imagenet"]:
            num_batches = int(1e5/args.bs)+1
        optimizer = EntropySGD(model.parameters(),
                    config = dict(lr=args.lr, num_batches=num_batches, gtime=args.gtime, momentum=args.mom, 
                    momentum_sgld=args.mom_sgld, nesterov=True, weight_decay=args.wd, L=args.L, eps=args.sgld_noise, 
                    g0=args.g, g1=args.grate, gmax=args.gmax, epochs=args.epochs, sgld_lr=args.sgld_lr, alpha_arg=args.alpha, 
                    gscale=args.gscale, modelname=args.model))
                    
    return optimizer