# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 12:30:49 2018
@author: akash
"""

import torch
import torch.nn as nn
from torch.autograd import Function

class BinarizeF(Function):

    @staticmethod
    def forward(cxt, input):
        output = input.new(input.size())
        output[input >= 0] = 1
        output[input < 0] = -1
        return output

    @staticmethod
    def backward(cxt, grad_output):
        grad_input = grad_output.clone()
        return grad_input

# aliases
binarize = BinarizeF.apply

# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 12:29:33 2018
@author: akash
"""

import math
import torch.nn.parameter as Parameter
import torch
import torch.nn as nn
import torch.nn.functional as F

#from functions import *


class BinaryTanh(nn.Module):
    def __init__(self):
        super(BinaryTanh, self).__init__()
        self.hardtanh = nn.Hardtanh()

    def forward(self, input):
        output = self.hardtanh(input)
        output = binarize(output)
        return output
        

class BinaryLinear(nn.Linear):

    def forward(self, input):
        #binary_weight = binarize(self.weight)
        if input.size(1) != 784:
            input.data=binarize(input.data)
        if not hasattr(self.weight,'org'):
            self.weight.org=self.weight.data.clone()
        self.weight.data=binarize(self.weight.org)
        out = nn.functional.linear(input, self.weight)
        if not self.bias is None:
            self.bias.org=self.bias.data.clone()
            out += self.bias.view(1, -1).expand_as(out)

        return out
        if self.bias is None:
            return F.linear(input, binary_weight)
        else:
            return F.linear(input, binary_weight, self.bias)

    def reset_parameters(self):
        # Glorot initialization
        in_features, out_features = self.weight.size()
        stdv = math.sqrt(1.5 / (in_features + out_features))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.zero_()

        self.weight.lr_scale = 1. / stdv

class BinConv2d(nn.Conv2d):

    def __init__(self, *kargs, **kwargs):
        super(BinConv2d, self).__init__(*kargs, **kwargs)


    def forward(self, input):
        if input.size(1) != 3:
            input.data = binarize(input.data)
        if not hasattr(self.weight,'org'):
            self.weight.org=self.weight.data.clone()
        self.weight.data=binarize(self.weight.org)

        out = nn.functional.conv2d(input, self.weight, None, self.stride,
                                   self.padding, self.dilation, self.groups)

        if not self.bias is None:
            self.bias.org=self.bias.data.clone()
            out += self.bias.view(1, -1, 1, 1).expand_as(out)

        return out
class Model(nn.Module):

    def __init__(self):
        super(Model, self).__init__()
        
        self.features = nn.Sequential(
            nn.Conv2d(3, 128 , kernel_size=3, padding=1,),
            nn.BatchNorm2d(128, eps=1e-4, momentum=0.1),
            nn.Hardtanh(),

            BinConv2d(128 , 128 , kernel_size=3, padding=1),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(128,eps=1e-4, momentum=0.1),
            nn.Hardtanh(),


            BinConv2d(128 , 256 , kernel_size=3, padding=1),
            nn.BatchNorm2d(256,eps=1e-4, momentum=0.1),
            nn.Hardtanh(),


            BinConv2d(256, 256 , kernel_size=3, padding=1),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(256,eps=1e-4, momentum=0.1 ),
            nn.Hardtanh(),


            BinConv2d(256, 512, kernel_size=3, padding=1),
            nn.BatchNorm2d(512,eps=1e-4, momentum=0.1),
            nn.Hardtanh(),


            nn.Conv2d(512, 512, kernel_size=3, padding=1),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(512,eps=1e-4, momentum=0.1),
            nn.Hardtanh()

        )
        self.classifier = nn.Sequential(
            BinaryLinear(512 * 4 * 4, 1024),
            nn.BatchNorm1d(1024),
            nn.Hardtanh(),
            #nn.Dropout(0.5),
            BinaryLinear(1024, 1024),
            nn.BatchNorm1d(1024),
            nn.Hardtanh(),
            #nn.Dropout(0.5),
            nn.Linear(1024, 10),
            nn.BatchNorm1d(10),
        )


    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, 512 * 4 * 4)
        x = self.classifier(x)
        return x