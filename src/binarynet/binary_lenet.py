import torch
import torch.nn as nn
import torch.nn.functional as F
from .modules import BinaryConv2d, BinaryLinear, BinaryTanh
from .binarized_modules import BinarizeConv2d, BinarizeLinear

# BinaryTanh = nn.Hardtanh

class BinaryLeNetLuci(nn.Module):
        
    def __init__(self, dim=32, in_channels=3):
        super(BinaryLeNet, self).__init__()
        self.conv1 = nn.Sequential(BinaryConv2d(in_channels, 20, 5, 1, bias=False),
                            nn.BatchNorm2d(20),
                            nn.MaxPool2d(2),
                            BinaryTanh())
        self.conv2 = nn.Sequential(BinaryConv2d(20, 50, 5, 1, bias=False),
                            nn.BatchNorm2d(50),
                            nn.MaxPool2d(2),
                            BinaryTanh())
        
        self.fc1 = nn.Sequential(BinaryLinear(800, 500, bias=False), # for MNIST and Fashion 1250->800
                            nn.BatchNorm1d(500),
                            BinaryTanh())
        
        self.fc2 = nn.Sequential(BinaryLinear(500, 10, bias=False),
                                nn.BatchNorm1d(10)
                                )

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = x.view(-1, 800)
        x = self.fc1(x)
        x = self.fc2(x)
        return x
    
class BinaryLeNet(nn.Module):
        
    def __init__(self, dim=32, in_channels=3):
        super(BinaryLeNet, self).__init__()
        self.conv1 = nn.Sequential(BinarizeConv2d(in_channels, 20, 5, 1, bias=False),
                            nn.BatchNorm2d(20),
                            nn.MaxPool2d(2),
                            nn.Hardtanh(inplace=True))
        self.conv2 = nn.Sequential(BinarizeConv2d(20, 50, 5, 1, bias=False),
                            nn.BatchNorm2d(50),
                            nn.MaxPool2d(2),
                            nn.Hardtanh(inplace=True))
        
        self.fc1 = nn.Sequential(BinarizeLinear(1250, 500, bias=False), # for MNIST and Fashion 1250->800
                            nn.BatchNorm1d(500),
                            nn.Hardtanh(inplace=True))
        
        self.fc2 = nn.Sequential(BinarizeLinear(500, 10, bias=False),
                                nn.BatchNorm1d(10)
                                )

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = x.view(-1, 1250)
        x = self.fc1(x)
        x = self.fc2(x)
        return x

class BinaryMLPpaper(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.infl_ratio=1
        self.fc1 = BinarizeLinear(784, 2048*self.infl_ratio)
        self.htanh1 = nn.Hardtanh()
        self.bn1 = nn.BatchNorm1d(2048*self.infl_ratio)
        self.fc2 = BinarizeLinear(2048*self.infl_ratio, 2048*self.infl_ratio)
        self.htanh2 = nn.Hardtanh()
        self.bn2 = nn.BatchNorm1d(2048*self.infl_ratio)
        self.fc3 = BinarizeLinear(2048*self.infl_ratio, 2048*self.infl_ratio)
        self.htanh3 = nn.Hardtanh()
        self.bn3 = nn.BatchNorm1d(2048*self.infl_ratio)
        self.fc4 = BinarizeLinear(2048*self.infl_ratio, 10)
        self.logsoftmax=nn.LogSoftmax()
        self.drop=nn.Dropout(0.5)

    def forward(self, x):
        x = x.view(-1, 28*28)
        x = self.fc1(x)
        x = self.bn1(x)
        x = self.htanh1(x)
        x = self.fc2(x)
        x = self.bn2(x)
        x = self.htanh2(x)
        x = self.fc3(x)
        x = self.drop(x)
        x = self.bn3(x)
        x = self.htanh3(x)
        x = self.fc4(x)
        return self.logsoftmax(x)
    
class BinaryMLP(nn.Module):
    def __init__(self, nin, nhs, nout):
        super(BinaryMLP, self).__init__()
        assert isinstance(nhs, list)

        self.layers = nn.Sequential(
            nn.Linear(nin, nhs[0]),
            nn.BatchNorm1d(nhs[0]),
            nn.ReLU(),
            *[nn.Linear(nhs[i//2], nhs[i//2 + 1]) if i%2==0 else nn.ReLU() for i in range(2*(len(nhs)-1))],
            nn.Linear(nhs[-1], nout))
        
    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        return self.layers(x)
