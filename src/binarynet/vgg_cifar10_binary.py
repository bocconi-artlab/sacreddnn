import torch
import torch.nn as nn
import torchvision.transforms as transforms
from torch.autograd import Function
from .binarized_modules import BinarizeLinear, BinarizeConv2d#, HardtanhBeta, TanhBeta

class VGG_Cifar10(nn.Module):

    def __init__(self, num_classes=10, dropout=0., beta=1.): # se c'è dropout ma non specificato è 0.5
        super(VGG_Cifar10, self).__init__()
        self.infl_ratio = 1
        self.mybias = True
        self.beta = beta   # hardtanh slope increase rate
        self.features = nn.Sequential(
            BinarizeConv2d(3, 128*self.infl_ratio, kernel_size=3, stride=1, padding=1, bias=self.mybias),
            nn.BatchNorm2d(128*self.infl_ratio),
            nn.Hardtanh(inplace=True),
            #nn.Dropout2d(0.1),

            BinarizeConv2d(128*self.infl_ratio, 128*self.infl_ratio, kernel_size=3, padding=1, bias=self.mybias),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(128*self.infl_ratio),
            nn.Hardtanh(inplace=True),
            #nn.Dropout2d(0.1),

            BinarizeConv2d(128*self.infl_ratio, 256*self.infl_ratio, kernel_size=3, padding=1, bias=self.mybias),
            nn.BatchNorm2d(256*self.infl_ratio),
            nn.Hardtanh(inplace=True),
            #nn.Dropout2d(0.1),

            BinarizeConv2d(256*self.infl_ratio, 256*self.infl_ratio, kernel_size=3, padding=1, bias=self.mybias),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(256*self.infl_ratio),
            nn.Hardtanh(inplace=True),
            #nn.Dropout2d(0.1),

            BinarizeConv2d(256*self.infl_ratio, 512*self.infl_ratio, kernel_size=3, padding=1, bias=self.mybias),
            nn.BatchNorm2d(512*self.infl_ratio),
            nn.Hardtanh(inplace=True),
            #nn.Dropout2d(0.1),

            BinarizeConv2d(512*self.infl_ratio, 512, kernel_size=3, padding=1, bias=self.mybias),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(512),
            nn.Hardtanh(inplace=True),
            nn.Dropout2d(dropout)

        )
        self.classifier = nn.Sequential(
            BinarizeLinear(512 * 4 * 4, 1024, bias=self.mybias),
            nn.BatchNorm1d(1024),
            nn.Hardtanh(inplace=True),
            nn.Dropout(dropout),
            BinarizeLinear(1024, 1024, bias=self.mybias),
            nn.BatchNorm1d(1024),
            nn.Hardtanh(inplace=True),
            nn.Dropout(dropout),

            ######################################################################
            # aggiunto due layer FC
            #BinarizeLinear(1024, 1024, bias=self.mybias),
            #nn.BatchNorm1d(1024),
            #nn.Hardtanh(inplace=True),
            ##nn.Dropout(0.5),
            #BinarizeLinear(1024, 1024, bias=self.mybias),
            #nn.BatchNorm1d(1024),
            #nn.Hardtanh(inplace=True),
            ##nn.Dropout(0.5),
            ######################################################################
            
            BinarizeLinear(1024, num_classes, bias=self.mybias),
            nn.BatchNorm1d(num_classes, affine=False)#,
            #nn.LogSoftmax()
        )

        self.regime = {
            0: {'optimizer': 'Adam', 'betas': (0.9, 0.999), 'lr': 5e-3},
            40: {'lr': 1e-3},
            80: {'lr': 5e-4},
            100: {'lr': 1e-4},
            120: {'lr': 5e-5},
            140: {'lr': 1e-5}
        }

    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, 512 * 4 * 4)
        x = self.classifier(x)
        return x

# used to reproduce BinaryNet's results
class VGG_Cifar10_new(nn.Module):

    def __init__(self, num_classes=10, dropout=0):
        super(VGG_Cifar10_new, self).__init__()
        self.mybias=True
        self.features = nn.Sequential(
            BinarizeConv2d(3, 128, kernel_size=3, stride=1, padding=1, bias=self.mybias),
            nn.BatchNorm2d(128, eps=1e-4, momentum=0.1),
            nn.Hardtanh(inplace=True),

            BinarizeConv2d(128, 128, kernel_size=3, padding=1, bias=self.mybias),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(128, eps=1e-4, momentum=0.1),
            nn.Hardtanh(inplace=True),

            BinarizeConv2d(128, 256, kernel_size=3, padding=1, bias=self.mybias),
            nn.BatchNorm2d(256, eps=1e-4, momentum=0.1),
            nn.Hardtanh(inplace=True),

            BinarizeConv2d(256, 256, kernel_size=3, padding=1, bias=self.mybias),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(256, eps=1e-4, momentum=0.1),
            nn.Hardtanh(inplace=True),

            BinarizeConv2d(256, 512, kernel_size=3, padding=1, bias=self.mybias),
            nn.BatchNorm2d(512, eps=1e-4, momentum=0.1),
            nn.Hardtanh(inplace=True),

            BinarizeConv2d(512, 512, kernel_size=3, padding=1, bias=self.mybias),
            nn.MaxPool2d(kernel_size=2),
            nn.BatchNorm2d(512, eps=1e-4, momentum=0.1),
            nn.Hardtanh(inplace=True)

        )
        self.classifier = nn.Sequential(
            BinarizeLinear(512 * 4 * 4, 1024, bias=self.mybias),
            nn.BatchNorm1d(1024, eps=1e-4, momentum=0.1),
            nn.Hardtanh(inplace=True),
            #nn.Dropout(0.5),
            BinarizeLinear(1024, 1024, bias=self.mybias),
            nn.BatchNorm1d(1024, eps=1e-4, momentum=0.1),
            nn.Hardtanh(inplace=True),
            #nn.Dropout(0.5),
            nn.Linear(1024, num_classes, bias=self.mybias),
            nn.BatchNorm1d(num_classes, eps=1e-4, momentum=0.1)
        )

        self.regime = {
            0: {'optimizer': 'Adam', 'betas': (0.9, 0.999), 'lr': 5e-3},
            40: {'lr': 1e-3},
            80: {'lr': 5e-4},
            100: {'lr': 1e-4},
            120: {'lr': 5e-5},
            140: {'lr': 1e-5}
        }

    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, 512 * 4 * 4)
        x = self.classifier(x)
        return x


def vgg_cifar10_binary(**kwargs):
    num_classes = kwargs.get('num_classes', 10)
    dropout = kwargs.get('dropout', 0)
    return VGG_Cifar10(num_classes, dropout)
