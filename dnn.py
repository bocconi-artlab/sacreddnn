# Basic script for NN training on standard classification datasets
import os
import torch

import argparse
import torch.optim as optim
from torch.utils.data import SubsetRandomSampler, DataLoader
import time
from tqdm import tqdm
import pandas as pd
import numpy as np
import tempfile
import random
from shutil import copyfile

from sacred import Experiment
from sacred.commands import print_config
ex = Experiment('DNN') # NOTE: this name should reflect the script name
from sacred import SETTINGS 
SETTINGS['CAPTURE_MODE'] = 'no' # don't capture output (avoid progress bar clutter)

# add sacreddnn dir to path (deprecated)
import os, sys
sacreddnn_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, sacreddnn_dir) 

# SacredDNN imports
from src.get_datasets import get_dataset
from src.get_losses import get_loss_function
from src.get_models import get_model_type
from src.get_optimizers import get_optimizer
from src.utils import set_beta, topk_acc, take_n_per_class, predict_outputs
from src.utils import num_params, l2_norm, run_and_config_to_path,\
                            file_observer_dir, to_gpuid_string
from src.models.custom_activations import replace_relu_
from src.data_transforms.cifar10_dataset import get_dataloader_adv
from src.warmup_scheduler import GradualWarmupScheduler

# SAM
from src.SAM.bn_bypass import disable_running_stats, enable_running_stats
# pruning
from iterative_pruning.iterative_magnitude_pruning import prune_magnitude_global


def train(loss, model, device, train_loader, teacher_train_pred, optimizer, args):
    model.train()
    t = tqdm(train_loader) # progress bar integration
    train_loss, accuracy, ndata = 0, 0, 0    
    for data, target, idx in t:
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()

        if args.opt == "SAM":
            enable_running_stats(model)

        output = model(data)

        if teacher_train_pred is None:
            l = loss(output, target)
        else:
            y_teacher = teacher_train_pred[idx].to(device)
            l = loss(output, target, y_teacher, args.alphaT, args.T)

        l.backward()

        if args.opt != "SAM":
            optimizer.step()
        else:
            optimizer.first_step(zero_grad=True)
            disable_running_stats(model)
            if teacher_train_pred is None:
                loss(model(data), target).backward()
            else:
                loss(model(data), target, y_teacher, args.alphaT, args.T).backward()

            optimizer.second_step(zero_grad=True)

        train_loss += l.item()*len(data)
        if args.loss != "mseregr":
            pred = output.argmax(dim=1, keepdim=True)
            accuracy += pred.eq(target.view_as(pred)).sum().item()
        ndata += len(data)
        
        t.set_postfix(loss=train_loss/ndata, err=100*(1-accuracy/ndata))

def eval_loss_and_error(loss, model, device, loader, teacher_pred, args):
    # t0 = time.time()
    model.eval()
    l, accuracy, ndata = 0, 0, 0
    with torch.no_grad():
        for data, target, idx in loader:
            # print(f"SHAPE {data.shape}")
            data, target = data.to(device), target.to(device)
            output = model(data)
            #if output.shape == target.shape: # TODO: WHY? E' SBAGLIATO <----------------
            
            if teacher_pred is None:
                l += loss(output, target, reduction='sum').item()
            else:
                y_teacher = teacher_pred[idx].to(device)
                l += loss(output, target, y_teacher, args.alphaT, args.T).item()*args.bs # is the reduction sum with this last multiplication?

            if args.loss != "mseregr":
                pred = output.argmax(dim=1, keepdim=True)
                accuracy += pred.eq(target.view_as(pred)).sum().item()
            ndata += len(data)
    # print(f"@eval time: {time.time()-t0:.4f}s")
    return l/ndata, (1-accuracy/ndata)*100

@ex.config  # Configuration is defined through local variables.
def cfg():
    bs             = 128            # input batch size for training
    epochs         = 100            # number of epochs to train
    lr             = 0.1            # learning rate
    wd             = 5e-4           # weight decay param (=L2 reg. Good value is 5e-4)
    mom            = 0.9            # momentum
    dropout        = 0.0            # dropout
    no_cuda        = False          # disables CUDA training
    nthreads       = 2              # number of threads
    save_model     = False          # save current model to path
    save_epoch     = 10             # save every save_epoch model
    keep_models    = False          # keep all saved models
    load_model     = ""             # load model from path
    last_epoch     = 0              # last_epoch for lr_scheduler
    droplr         = 5              # learning rate drop factor (use 0 for no-drop)
    drop_mstones   = "drop_150_225" # learning rate milestones (epochs at which applying the drop factor)
    opt            = "nesterov"     # optimizer type
    loss           = "nll"          # classification loss [nll, mse, mseregr, hinge]
    warmup         = False          # GradualWarmupScheduler
    model          = "lenet"        # model type  [lenet, densenet, resnet_cifar, efficientnet-b{1-7}(-pretrained)]  
    bias           = True           # learn a bias
    dataset        = "fashion"      # dataset  [mnist, fashion, cifar10, cifar100]
    adv            = False          # adversarial initialization: set random labels on the training set
    datapath       = '~/data/'      # folder containing the datasets (e.g. mnist will be in "data/MNIST")
    logtime        = 2              # report every logtime epochs
    #M = -1                # take only first M training examples 
    #Mtest = -1            # take only first Mtest test examples 
    pclass = -1                    # take only pclass training examples for each class 
    preproc        = 0              # data preprocessing level. preprocess=0 is no preproc. Check preproc_transforms.py
    gpu            = 0              # which gpu(s) to use, if 'distribute' all gpus are used
    deterministic  = True           # set deterministic mode on gpu
    noise_ampl     = 0.             # noise amplitude (for perturbing model initialization)
    init_rescale   = 0.             # rescaling of model initialization
    earlystop      = False          # stop training when a configuration at zero training error is found
    images         = False          # generate images from teacher

    k              = 1              # multiplier for lr warmup
    
    # non-trivial data augmentations ((Fast)AutoAugment, CutOut)
    augmentation_type = 'autoaug_cifar10'
    cutout         = 0

    # SAM hyperparameters
    rho = 0.05

    # Knowledge Distillation hyperparameters
    load_teacher = ""
    alphaT       = 1.0
    T            = 4.0

    # PRUNING
    prune_rep  = 0
    prune_init = 4
    prune_ep   = 160
    prune_pct  = 0.2

    # Entropy-SGD hyperparameters
    L              = 1
    sgld_noise     = 1e-4
    #sgld_g = 1e-4
    #sgld_grate = 1e-3
    
    ## CHANGE ACTIVATIONS
    act            = None   # Change to e.g. "swish" to replace each relu of the model with a new activation
                            # ["swish", "quadu", "mish", ...]

    # To be done before any call to torch.cuda
    os.environ["CUDA_VISIBLE_DEVICES"] = to_gpuid_string(gpu)
    
@ex.automain
def main(_run, _config):

    args = argparse.Namespace(**_config)
    print_config(_run); print()
    logdir = file_observer_dir(_run)
    if logdir is not None:
        from torch.utils.tensorboard import SummaryWriter
        writer = SummaryWriter(log_dir=f"{logdir}/{run_and_config_to_path(_run, _config)}")
    if args.save_model: # make temp file. In the end, the model will be stored by the observers.
        save_prefix = tempfile.mkdtemp() + "/model"

    use_cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device(args.gpu) if use_cuda else torch.device("cpu")
    torch.set_num_threads(args.nthreads)
    print(f"USE_CUDA = {use_cuda}, DEVICE_COUNT={torch.cuda.device_count()}, NUM_CPU_THREADS={torch.get_num_threads()}")
    
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)
    if args.deterministic:
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    
    # DATASET ---------------------------------------------------------------------
    loader_args = {'pin_memory': True} if use_cuda else {}
    #dtrain, dtest = get_dataset(args) if not args.adv else get_dataloader_adv(args)
    dtrain, dtest = get_dataset(args)
    train_idxs = list(range(len(dtrain))) if args.pclass<=0 else take_n_per_class(dtrain, args.pclass)
    test_idxs = list(range(len(dtest)))
    
    # randomize labels
    if args.adv:
        print("RANDOMIZE LABELS")
        for i in range(len(dtrain.targets)):
            dtrain.targets[i] = random.randint(0, 9)
    
    if args.images:
        for i in range(len(dtrain.targets)):
            if args.dataset in ["mnist", "fashion"]:
                #dtrain.data[i].fill_(255)
                dtrain.data[i].fill_(1)
            else:
                #dtrain.data[i].fill(1)
                dtrain.data[i].fill(1)
            dtrain.targets[i] = 0
            # TODO: ma le immagini sono tra 0 e 256 ??
            #print(dtrain.data[i])
            #raise NameError
    
    # MODEL ---------------------------------------------------------------------
    Net = get_model_type(args)
    model = Net().to(device)
    #if use_cuda and torch.cuda.device_count() > 1:
    #    model = torch.nn.DataParallel(model)
    if args.load_model:
        model.load_state_dict(torch.load(args.load_model))
    #replace activation function
    replace_relu_(model, args.act)

    # DATASET --------------------------------------------------------------------
    # GENERATING IMAGES FROM TEACHER
    if args.images:
        # freeze the network
        for param in model.parameters():
            #print(param.size())
            param.requires_grad = False
        #####################################################################
        # take teacher outputs as labels
        output_targets = False
        if output_targets:
            for i in range(len(dtrain.targets)):
                with torch.no_grad():
                    img = torch.Tensor(np.transpose(dtrain.data[i], (2,0,1)))
                    outputs = model(img.view(1,3,32,32).to(device))
                    dtrain.targets[i] = outputs.view(-1).to("cpu")
            for i in range(len(dtrain.targets)):
                dtrain.targets[i] = dtrain.targets[123]
        #####################################################################
        args.model = "mlpimages"   if args.model.startswith("mlp") else \
                     "lenetimages" if args.model == "lenet" else None            
        Net = get_model_type(args, model)
        model = Net().to(device)
    
    print(model)
    ex.info["num_params"] = num_params(model)
    ex.info["num_learnable"] = num_params(model, learnable=True)
    print(f"MODEL: {ex.info['num_params']} params, {ex.info['num_learnable']} learnable")

    #############################
    #train_loader = DataLoader(dtrain, sampler=SubsetRandomSampler(train_idxs),
    #                          batch_size=args.bs, **loader_args)
    #test_loader  = DataLoader(dtest, sampler=SubsetRandomSampler(test_idxs),
    #                         batch_size=args.bs, **loader_args)
    train_loader = DataLoader(dataset=dtrain, batch_size=args.bs, shuffle=True, **loader_args)
    test_loader = DataLoader(dataset=dtest, batch_size=args.bs,shuffle=False, **loader_args)

    print(f"DATASET {args.dataset}: {len(train_idxs)} Train and {len(test_idxs)} Test examples")

    if args.load_teacher:
        teacher_model = Net().to(device)
        teacher_model.load_state_dict(torch.load(args.load_teacher))
        train_loader_eval = DataLoader(dataset=dtrain, batch_size=args.bs, shuffle=False, **loader_args)
        teacher_train_pred = predict_outputs(teacher_model, train_loader_eval, device)
        teacher_test_pred = predict_outputs(teacher_model, test_loader, device)
    else:
        teacher_train_pred, teacher_test_pred = None, None

    # OPTIMIZER -------------------------------------------------------------------------
    optimizer = get_optimizer(args, model)
    if args.opt == "SAM":
        scheduler_optimizer = optimizer.base_optimizer
    else:
        scheduler_optimizer = optimizer

    # LOSS FUNCTION ---------------------------------------------------------------------
    loss = get_loss_function(args)

    # SCHEDULERS ------------------------------------------------------------------------
    if args.droplr: #(mettere in una funzione)
        if args.droplr == 'cosine':
            print("Cosine lr schedule")

            scheduler = optim.lr_scheduler.CosineAnnealingLR(scheduler_optimizer, T_max=args.epochs, eta_min=0., last_epoch=-1)

            #scheduler = optim.lr_scheduler.CosineAnnealingLR(scheduler_optimizer, T_max=args.epochs)

        elif args.droplr < 1. and args.droplr != 0.:
            scheduler = optim.lr_scheduler.StepLR(scheduler_optimizer, step_size=1, gamma=args.droplr)
        else:
            gamma_sched = 1. / args.droplr if args.droplr > 0 else 1
            if args.drop_mstones is not None:
                mstones = [int(h) for h in args.drop_mstones.split('_')[1:]]
            else:
                mstones = [args.epochs//2, args.epochs*3//4, args.epochs*15//16]
            print("MileStones %s" % mstones)
            scheduler = optim.lr_scheduler.MultiStepLR(scheduler_optimizer, milestones=mstones, gamma=gamma_sched)

    if args.warmup:
        scheduler = GradualWarmupScheduler(scheduler_optimizer, multiplier=args.k, total_epoch=5, 
                                           after_scheduler=scheduler)

    # dummy loop to make the schedulers progress (deprecated)
    for i in range(args.last_epoch):
        scheduler.step()

    # REPORT -------------------------------------------------------------------------
    def report(epoch):
        model.eval()
        o = dict() # store observations
        o["epoch"] = epoch
        o["lr"] = optimizer.param_groups[0]["lr"]
        o["train_loss"], o["train_error"] = \
            eval_loss_and_error(loss, model, device, train_loader, teacher_train_pred, args)
        o["test_loss"], o["test_error"] = \
            eval_loss_and_error(loss, model, device, test_loader, teacher_test_pred, args)
        o["norms"]  = l2_norm(model)
        print("\n", pd.DataFrame({k:[o[k]] for k in o}), "\n")
        for k in o:
            ex.log_scalar(k, o[k], epoch)
            if logdir:
                writer.add_scalar(k, o[k], epoch)
        return o["train_error"]
        
    # TRAINING -----------------------------------------------------------------------
    train_error = report(args.last_epoch)

    # PRUNE
    if args.prune_rep > 0:
        args.epochs *= (args.prune_rep+1)
        for epoch in range(0, args.prune_init):
            train(loss, model, device, train_loader, teacher_train_pred, optimizer, args)
        rewind_state = model.state_dict()
    ##

    for epoch in range(args.last_epoch + 1, args.epochs + 1):

        train(loss, model, device, train_loader, teacher_train_pred, optimizer, args)

        # torch.cuda.empty_cache()
        if epoch % args.logtime == 0:
            train_error = report(epoch)
            
        if epoch % args.save_epoch and args.save_model:
            model_path = save_prefix+".pt"
            torch.save(model.state_dict(), model_path)
            if args.keep_models:
                kept_model_path = save_prefix+"_epoch_{}.pt".format(epoch)
                copyfile(model_path, kept_model_path)
                ex.add_artifact(kept_model_path)#, content_type="application/octet-stream")
            ex.add_artifact(model_path)#, content_type="application/octet-stream")   
            
        if args.droplr:
            print("lr=%s" % scheduler.get_last_lr())
            scheduler.step()

        if args.earlystop and train_error == 0.0:
            break
            
        # PRUNING
        if args.prune_rep > 0 and epoch < args.epochs-args.prune_ep//2 and epoch % args.prune_ep:
            prune_magnitude_global(model, rewind_state, pct=args.prune_pct)
            # reset schedulers

    # AFTER TRAINING
    # Save model
    if args.save_model:
        model_path = save_prefix+"_final.pt"
        torch.save(model.state_dict(), model_path)
        ex.add_artifact(model_path)#, content_type="application/octet-stream")

        
